<?php

if (isset($_REQUEST['item'])) {
    $item_url = 'https://participatory-archives.ch/api/items/' . $_REQUEST['item'];
    $item = json_decode(file_get_contents($item_url), true);

    /* foreach ($item as $key => $value) {
        if (str_contains($key, 'schema')) {
            print($key . '<br>');
            foreach ($value as $subkey => $subvalue) {
                if (isset($subvalue['@id'])) {
                    print('<bold>' . $subvalue['@id'] . '</bold><br>');
                }
                if (isset($subvalue['@value']) && $key != 'schema:geo') {
                    print('<i>' . $subvalue['@value'] . '</i><br>');
                }
            }
        }
    } */
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PIA Graph UI</title>

    <link rel="stylesheet" href="dist/output.css">

</head>

<body>

    <main>
        <h1 class="text-2xl font-bold">PIA Graph UI</h1>
        <div id="sigma-container"></div>
    </main>
    
    <script src="build/bundle.js"></script>
    
    <!-- <script>
        <?php if (isset($_REQUEST['item'])) : ?>

            document.addEventListener('DOMContentLoaded', () => {
                let data = {
                    container: document.getElementById('cy'),
                    elements: [
                        {
                            data: {
                                id: "<?= $item['o:id'] ?>"
                            }
                        },
                        <?php foreach ($item as $key => $value) : ?>
                            <?php if (str_contains($key, 'schema')) : ?>
                                <?php foreach ($value as $subkey => $subvalue) :  ?>
                                    <?php if (isset($subvalue['value_resource_id'])) :  ?>
                                        {
                                            data: {
                                                id: "<?= $subvalue['display_title'] ?>",
                                            }
                                        },
                                        {
                                            data: {
                                                id: "<?= $item['o:id'] ?>_<?= $subvalue['display_title'] ?? $subvalue['@id'] ?>",
                                                source: "<?= $item['o:id'] ?>",
                                                target: "<?= $subvalue['display_title'] ?? $subvalue['@id'] ?>",
                                            }
                                        },
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    ],
                    style: [ // the stylesheet for the graph
                        {
                            selector: 'node',
                            style: {
                                'background-color': '#666',
                                'label': 'data(id)'
                            }
                        },

                        {
                            selector: 'edge',
                            style: {
                                'width': 3,
                                'line-color': '#ccc',
                                'target-arrow-color': '#ccc',
                                'target-arrow-shape': 'triangle',
                                'curve-style': 'bezier'
                            }
                        }
                    ],

                    layout: {
                        name: 'concentric',
                    }
                });
            };

        <?php endif; ?>
    </script> -->
</body>

</html>